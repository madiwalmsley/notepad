create table notes
(
    id serial primary key,
    note text,
    date TIMESTAMP DEFAULT Now()
);