const express = require('express');
const bodyParser = require('body-parser');
const massive = require('massive');
const cors = require('cors');
require('dotenv').config();

const notepadController = require('./controllers/notepad_controller');

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.use(express.static(__dirname));
if (process.env.NODE_ENV === 'production') { app.use(express.static(`${__dirname}/../build`)); }

massive(process.env.CONNECTION_STRING).then(database => {
    app.set('db', database);
}).catch(err => {
    console.log('Error w massive', err);
})

// notepad controller
app.get('/notes', notepadController.getNotes);
app.get('/notes/:id', notepadController.getNote);
app.post('/notes', notepadController.addNote);
app.put('/notes/:id', notepadController.editNote);
app.delete('/notes/:id', notepadController.deleteNote);

const port = process.env.PORT || 4000;

app.listen(port, () => {
    console.log(`App is listening on port ${port}~~~~~`);
})

const path = require('path');
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'));
});
