module.exports = {
    getNotes: (req, res) => {
        req.app.get('db').get_notes().then(notes => {
            res.status(200).send(notes);
        }).catch(error => {
            res.status(400).send('Error getting notes');
            console.log('error getting items', error);
        })
    },
    getNote: (req, res) => {
        const { id } = req.params;
        req.app.get('db').get_note([id]).then(note => {
            res.status(200).send(note);
        }).catch(error => {
            console.lof('error getting note', error);
        })
    },
    addNote: (req, res) => {
        const { note } = req.body;
        req.app.get('db').add_note([note]).then(notes => {
            res.status(200).send(notes);
        }).catch(error => {
            console.log('error adding note', error);
        })
    },
    editNote: (req, res) => {
        const { id } = req.params;
        const { note } = req.body;
        req.app.get('db').edit_note([note, id]).then(notes => {
            res.status(200).send(notes);
        }).catch(error => {
            console.log('error editing note', error);
        })
    },
    deleteNote: (req, res) => {
        const { id } = req.params;
        req.app.get('db').delete_note([id]).then(notes => {
            res.status(200).send(notes);
        }).catch(error => {
            console.log('error deleting note', error);
        })
    }
}