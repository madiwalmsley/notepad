const INITIAL_STATE = {
    notes: [],
    id: null
}

const ADD_NOTE = 'ADD_NOTE';
const DELETE_NOTE = 'DELETE_NOTE';
const EDIT_NOTE = 'EDIT_NOTE';
const SELECTED_NOTE = 'SELECTED_NOTE';

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case ADD_NOTE:
            return { ...state, notes: [...action.payload] };
        case DELETE_NOTE:
            return { ...state, notes: [...action.payload] };
        case EDIT_NOTE:
            return { ...state, notes: [...action.payload] };
        case SELECTED_NOTE:
            return { ...state, id: action.payload };
        default:
            return state;
    }
}

export function addNote(note) {
    return {
        type: ADD_NOTE,
        payload: note
    }
}
export function deleteNote(note) {
    return {
        type: DELETE_NOTE,
        payload: note
    }
}
export function editNote(note) {
    return {
        type: EDIT_NOTE,
        payload: note
    }
}

export function selectNote(noteId) {
    return {
        type: SELECTED_NOTE,
        payload: noteId
    }
}