import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { addNote, deleteNote, selectNote } from '../../ducks/reducer';
import './List.scss';

class List extends Component {
    constructor() {
        super();
        this.state = {
        }
    }

    componentDidMount() {
        axios.get(`/notes`).then(res => {
            this.props.addNote(res.data);
        })
    }

    view = (id) => {
        this.props.selectNote(id);
    }

    render() {
        const listView = this.props.notes.map(note => {
            const date = new Date(note.date);
            return <li key={note.id} className="note" onClick={() => this.view(note.id)}>
                <h2 className="date">{date.toDateString()}</h2>
                <p className="preview">{note.note}</p>
            </li>
        })
        return (
            <div className="list-container">
                <h1>Notes</h1>
                {this.props.notes.length >= 1 ? <ul className="list"> {listView} </ul> : <div className="list">No notes to display</div>}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        notes: state.notes,
        id: state.id
    }
}
export default connect(mapStateToProps, { addNote, deleteNote, selectNote })(List);