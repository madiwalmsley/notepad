import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteNote, selectNote, editNote } from '../../ducks/reducer';
import './NoteView.scss';
import axios from 'axios';

class NoteView extends Component {
    constructor() {
        super();
        this.state = {
            edit: ""
        }
    }
    componentDidMount() {
        const selected = this.props.notes.filter(note => note.id === this.props.id);
        this.setState({
            edit: selected[0].note
        })
    }
    componentDidUpdate(prevProps, prevState) {
        const selected = this.props.notes.filter(note => note.id === this.props.id);
        if (prevProps.id !== this.props.id) {
            this.setState({
                edit: selected[0].note
            })
        }
    }
    saveEdit = (id) => {
        axios.put(`/notes/${id}`, { note: this.state.edit }).then(res => {
            this.props.editNote(res.data);
            this.props.selectNote(null);
        })
    }
    removeNote = (id) => {
        axios.delete(`/notes/${id}`).then(res => {
            this.props.deleteNote(res.data);
            this.props.selectNote(null);
        })
    }
    render() {
        return (
            <div className="notepad-container">
                <h1>Edit Note</h1>
                <textarea autoFocus className="notepad" value={this.state.edit} onChange={(e) => this.setState({ edit: e.target.value })}></textarea>
                <div className="edit-nav">
                    <button className="edit" onClick={() => this.removeNote(this.props.id)}>Delete</button>
                    <button className="edit" onClick={() => this.props.selectNote(null)}>New Note</button>
                    <button onClick={() => this.saveEdit(this.props.id)} className={this.state.edit.length < 1 ? "edit" : "edit-ready"} disabled={!this.state.edit}>Save</button>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        notes: state.notes,
        id: state.id
    }
}

export default connect(mapStateToProps, { deleteNote, selectNote, editNote })(NoteView);