import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addNote } from '../../ducks/reducer';
import './Notepad.scss';
import axios from 'axios';
import NoteView from '../NoteView/NoteView';
import { isNull } from 'util';

class Notepad extends Component {
    constructor() {
        super();
        this.state = {
            note: ""
        }
    }
    recordNotes = (e) => {
        this.setState({
            note: e.target.value
        })
    }

    saveNote = () => {
        axios.post(`/notes`, { note: this.state.note }).then(res => {
            this.props.addNote(res.data);
        })
        this.setState({
            note: ""
        })
    }
    render() {
        return (
            isNull(this.props.id) ?
                <div className="notepad-container">
                    <h1>New Note</h1>
                    <textarea onChange={this.recordNotes} className="notepad" value={this.state.note} placeholder="Write some notes here :)"></textarea>
                    <button onClick={this.saveNote} className={this.state.note.length < 1 ? "save" : "save-ready"} disabled={!this.state.note}>Save</button>
                </div>
                :
                <NoteView />
        );
    }
}

function mapStateToProps(state) {
    return {
        notes: state.notes,
        id: state.id
    }
}

export default connect(mapStateToProps, { addNote })(Notepad);