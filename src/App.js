import React from 'react';
import './App.scss';
import List from './components/List/List';
import Notepad from './components/Notepad/Notepad';

function App() {
  return (
    <div className="App">
      <div className="notes-container">
        <List />
        <Notepad />
      </div>
    </div>
  );
}

export default App;
